#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import functools
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 0)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		tokens = line.strip().split()
		o.append((list(c for c in tokens[0]), int(tokens[1])))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def comparator(h1, h2):
	hand1 = h1[0]
	hand2 = h2[0]
	ch1 = Counter(hand1)
	ch2 = Counter(hand2)
	t1 = get_type(ch1)
	t2 = get_type(ch2)
	if t1 != t2:
		return t1 - t2
	for i in range(5):
		if hand1[i] != hand2[i]:
			return subtr(hand1[i], hand2[i])

def conv(h1):
	if h1 in "0123456789":
		h1 = int(h1)
	elif h1 == "A":
		h1 = 14
	elif h1 == "K":
		h1 = 13
	elif h1 == "Q":
		h1 = 12
	elif h1 == "J":
		h1 = 11
	elif h1 == "T":
		h1 = 10
	return h1

def subtr(h1, h2):
	return conv(h1) - conv(h2)

def get_type(hand):
	if len(hand) == 1:
		return 5
	elif len(hand) == 2 and 4 in hand.values():
		return 4
	elif len(hand) == 2 and 3 in hand.values():
		return 3
	elif len(hand) == 3 and 3 in hand.values():
		return 2
	elif len(hand) == 3:
		return 1
	elif len(hand) == 4:
		return 0
	return -1

def part1(inp):
	l = sorted(inp, key=functools.cmp_to_key(comparator))
	o = 0
	for i, p in enumerate(l):
		o += (1 + i) * p[1]
	return o

# not 250917800
def conv2(h1):
	if h1 in "0123456789":
		h1 = int(h1)
	elif h1 == "A":
		h1 = 14
	elif h1 == "K":
		h1 = 13
	elif h1 == "Q":
		h1 = 12
	elif h1 == "J":
		h1 = -1
	elif h1 == "T":
		h1 = 10
	return h1

def subtr2(h1, h2):
	return conv2(h1) - conv2(h2)

def comparator2(h1, h2):
	hand1 = h1[0]
	hand2 = h2[0]
	ch1 = Counter(hand1)
	ch2 = Counter(hand2)
	t1 = get_type2(ch1)
	t2 = get_type2(ch2)
	if t1 != t2:
		return t1 - t2
	for i in range(5):
		if hand1[i] != hand2[i]:
			return subtr2(hand1[i], hand2[i])

def get_type2(hand):
	if "J" not in hand.keys():
		if len(hand) == 1:
			return 5
		elif len(hand) == 2 and 4 in hand.values():
			return 4
		elif len(hand) == 2 and 3 in hand.values():
			return 3
		elif len(hand) == 3 and 3 in hand.values():
			return 2
		elif len(hand) == 3:
			return 1
		elif len(hand) == 4:
			return 0
		return -1
	else:
		if hand["J"] == 5:
			return 5
		elif hand["J"] == 4:
			return 5
		elif hand["J"] == 3:
			if len(hand) == 2:
				return 5
			else:
				return 4
		#5oak
		if len(hand) == 2:
			return 5
		elif len(hand) == 3 and (4 - hand["J"]) in hand.values():
			return 4
		elif len(hand) == 3 and ((3 - hand["J"]) in hand.values() or (2 - hand["J"]) in hand.values()):
			return 3
		elif len(hand) == 4 and (3 - hand["J"]) in hand.values():
			return 2
		elif len(hand) == 4 and (2 - hand["J"]) in hand.values():
			return 1
		elif len(hand) == 5:
			return 0
		return -1

def part2(inp):
	l = sorted(inp, key=functools.cmp_to_key(comparator2))
	o = 0
	for i, p in enumerate(l):
		o += (1 + i) * p[1]
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
