#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import random
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 54)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = {}
	for idx, line in enumerate(inp):
		t = line.strip().split()
		o[t[0][:-1]] = list(t[1:])
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp): # Drawn from https://old.reddit.com/r/adventofcode/comments/18qbsxs/2023_day_25_solutions/keuafrl/
	n = list(inp.keys())
	d = defaultdict(list)
	for k, v in inp.items():
		for node in v:
			d[k].append(graph.Edge(node))
			d[node].append(graph.Edge(k))
			if node not in n:
				n.append(node)
	g = graph.Graph(n, d)


	c = Counter()
	for _ in range(1000):
		n = random.choice(g.nodes)
		n2 = random.choice(g.nodes)
		if n == n2:
			continue
		queue = [n]
		seen = set()
		tree = defaultdict(list)
		while len(queue) > 0:
			curr = queue.pop(0)
			if curr == n2:
				break
			seen.add(curr)
			for e in g.adj[curr]:
				if e.dest in seen:
					continue
				seen.add(e.dest)
				queue.append(e.dest)
				tree[curr].append(e.dest)
	
		path = []
		curr = n2
		while curr != n:
			for k, v in tree.items():
				if curr in v:
					path.append(k)
					curr = k
		path.reverse()
		for i in range(len(path) - 1):
			c[tuple(sorted((path[i], path[i + 1])))] += 1
	for e, _ in c.most_common(3):
		for s in (e, reversed(e)):
			start, end = s
			ne = []
			for edge in g.adj[start]:
				if edge.dest != end:
					ne.append(edge)
			g.adj[start] = ne
	for n in g.nodes:
		queue = [n]
		seen = set()
		while len(queue) > 0:
			curr = queue.pop()
			seen.add(curr)
			for e in g.adj[curr]:
				if e.dest in seen:
					continue
				seen.add(e.dest)
				queue.append(e.dest)
				c[(curr, e.dest)] += 1
		return len(seen) * (len(g.nodes) - len(seen))
	return 0

def part2(inp):
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
