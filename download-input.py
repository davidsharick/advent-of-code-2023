#!/usr/bin/env python3

import sys
import requests

def main():
    day = int(sys.argv[2])
    year = sys.argv[1]
    with open("../config") as f:
        session = f.readline().strip()
    cookies = {"session": session}
    headers = {"User-Agent": f"David Sharick Script https://gitlab.com/davidsharick/advent-of-code-{year} david.le.sharick@gmail.com"}
    r = requests.get(f"https://adventofcode.com/{year}/day/{day}/input", cookies = cookies, headers = headers)
    with open(f"day{day}/day{day}-input.txt", "w") as f:
        f.write(r.text)


if __name__ == '__main__':
    main()
