#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 114)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 2)
		pass

def parse_input(inp, p2mode = False):
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	return sum(extrapolate(l) for l in inp)

def extrapolate(l):
	h = []
	while True:
		nl = []
		for i in range(len(l) - 1):
			nl.append(l[i + 1] - l[i])
		h.append(l)
		l = nl
		
		if len(Counter(nl)) == 1:
			extraped = nl[0]
			for r in reversed(h):
				new = r[-1] + extraped
				extraped = new
			return extraped

def extrapolate2(l):
	h = []
	while True:
		nl = []
		for i in range(len(l) - 1):
			nl.append(l[i + 1] - l[i])
		h.append(l)
		l = nl
		
		if len(Counter(nl)) == 1:
			extraped = nl[0]
			for r in reversed(h):
				new = r[0] - extraped
				extraped = new
			return extraped

def part2(inp):
	return sum(extrapolate2(l) for l in inp)

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
