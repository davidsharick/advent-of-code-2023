#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"""
		self.test = parse_input(self.testinput.split("\n"), 7)

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"), 7)
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 13)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 30)
		pass

def parse_input(inp, splitpoint, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		tokens = line.strip().split()
		o.append((list(int(t) for t in tokens[2:splitpoint]), list(int(t) for t in tokens[splitpoint + 1:])))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	o = 0
	for win, have in inp:
		i = 0
		for n in have:
			if n in win:
				i += 1
		if i == 0:
			o += 0
		else:
			o += 2 ** (i - 1)
	return o
	

def part2(inp):
	amounts = Counter()
	for i in range(1, len(inp) + 1):
		amounts[i] = 1
	for idx, card in enumerate(inp):
		win, have = card
		ridx = idx + 1
		add = 0
		for n in have:
			if n in win:
				add += 1
		for i in range(ridx + 1, min(ridx + add + 1, len(inp) + 1)):
			amounts[i] += amounts[ridx]
	return sum(amounts.values())

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), 12)
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), 12, True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
