#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """.|...\\....
|.-.\\.....
.....|-...
........|.
..........
.........\\
..../.\\\\..
.-.-/..|..
.|....-|.\\
..//.|...."""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 46)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 51)
		pass

def parse_input(inp, p2mode = False):
	return (readin.grid_set_input(inp, "/"), readin.grid_set_input(inp, "\\"), readin.grid_set_input(inp, "-"), readin.grid_set_input(inp, "|"))
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def fslash(d):
	if d == direc.UP:
		return direc.RIGHT
	elif d == direc.DOWN:
		return direc.LEFT
	elif d == direc.RIGHT:
		return direc.UP
	elif d == direc.LEFT:
		return direc.DOWN

def bslash(d):
	if d == direc.UP:
		return direc.LEFT
	elif d == direc.DOWN:
		return direc.RIGHT
	elif d == direc.RIGHT:
		return direc.DOWN
	elif d == direc.LEFT:
		return direc.UP

def part1(inp):
	return proc(inp, (0, 0), direc.RIGHT)

def proc(inp, sp, sd):
	fslashes, bslashes, dash, bar = inp
	if sp in fslashes:
		sd = fslash(sd)
	elif sp in bslashes:
		sd = bslash(sd)
	if sp not in dash and sp not in bar:
		beam = [sp, sd]
		beams = [beam]
	elif sp in dash:
		if sd in (direc.UP, direc.DOWN):
			beams = [[sp, direc.LEFT], [sp, direc.RIGHT]]
		else:
			beams = [[sp, sd]]
	elif sp in bar:
		if sd in (direc.RIGHT, direc.LEFT):
			beams = [[sp, direc.UP], [sp, direc.DOWN]]
		else:
			beams = [[sp, sd]]
	energized = set()
	my = max(max(l[0] for l in i) for i in inp)
	mx = max(max(l[1] for l in i) for i in inp)
	seen = set()
	while len(beams) > 0:
		nb = []
		for b in beams:
			p, d = b
			assert d in (direc.UP, direc.DOWN, direc.LEFT, direc.RIGHT)
			if (tuple(p), d) in seen:
				continue
			seen.add((tuple(p), d))
			energized.add(tuple(p))
			np = [p[0] + d[0], p[1] + d[1]]
			if np[0] < 0 or np[0] > my or np[1] < 0 or np[1] > mx:
				continue
			energized.add(tuple(np))
			if tuple(np) in fslashes:
				nb.append([np, fslash(d)])
			elif tuple(np) in bslashes:
				nb.append([np, bslash(d)])
			elif tuple(np) in dash:
				if d in (direc.DOWN, direc.UP):
					nb.append([np, direc.LEFT])
					nb.append([np, direc.RIGHT])
				else:
					nb.append([np, d])
			elif tuple(np) in bar:
				if d in (direc.LEFT, direc.RIGHT):
					nb.append([np, direc.UP])
					nb.append([np, direc.DOWN])
				else:
					nb.append([np, d])
			else:
				nb.append([np, d])
		beams = nb
	assert all(p[0] <= my and p[1] <= mx and p[0] >= 0 and p[1] >= 0 for p in energized)
	o = 0
	for y in range(my + 1):
		s = ""
		for x in range(mx + 1):
			if (y, x) in energized:
				s += "#"
			else:
				s += "."
		#pr(s)
		o += s.count("#")
	return len(energized)

def part2(inp):
	y0s = []
	x0s = []
	yms = []
	xms = []
	my = max(max(l[0] for l in i) for i in inp)
	mx = max(max(l[1] for l in i) for i in inp)
	for y in range(my + 1):
		x0s.append((y, 0))
		xms.append((y, mx))
	for x in range(mx + 1):
		y0s.append((0, x))
		yms.append((my, x))
	b = 0
	for p in y0s:
		b = max(b, proc(inp, p, direc.DOWN))
	for p in yms:
		b = max(b, proc(inp, p, direc.UP))
	for p in x0s:
		b = max(b, proc(inp, p, direc.RIGHT))
	for p in xms:
		b = max(b, proc(inp, p, direc.LEFT))
	return b

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
