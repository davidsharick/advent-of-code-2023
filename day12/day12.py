#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """#.#????##.###?? 1,1,2,3"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 21)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 525152)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		tokens = line.strip().split()
		springs = list(tokens[0])
		runs = list(int(c) for c in tokens[1].split(","))
		o.append((springs, runs))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	o = 0
	i = 0
	for spring, run in inp:
		o += recursive_calc(tuple(spring), tuple(run), 0, 0)#num_opts(spring, run)
		i += 1
	return o

def num_opts(springs, run):
	c = Counter(springs)
	qs = []
	for i in range(len(springs)):
		if springs[i] == "?":
			qs.append(i)
	o = 0
	assert len(qs) == c["?"]
	for i in range(2 ** c["?"]):
		spr = springs.copy()
		for exp in range(c["?"]):
			if (2 ** exp) & i:
				spr[qs[exp]] = "#"
			else:
				spr[qs[exp]] = "."
		if validate(spr, run):
			o += 1
	return o

def validate(springs, run):
	assert "?" not in springs
	runs = []
	cr = 0
	for i, c in enumerate(springs):
		if c == "#":
			cr += 1
		else:
			if cr != 0:
				runs.append(cr)
			cr = 0
	if cr != 0:
		runs.append(cr)
	return runs == run


def num_opts2(springs, runs):
	runs = tuple(runs * 5)
	springs = tuple(springs + ["?"] + springs + ["?"] + springs + ["?"] + springs + ["?"] + springs)
	opts = 1
	#dpt = defaultdict(dict)
	x = recursive_calc(tuple(springs), tuple(runs), 0, 0)
	return x

@lru_cache(None)
def recursive_calc(springs, runs, sidx, ridx):
	if sidx != 0:
		assert sidx - 1 >= len(springs) or springs[sidx - 1] != "#"
	# try to match first run
	if ridx >= len(runs):
		if sidx <= len(springs) + 1:
			# ensure there's no leftovers
			if not any(c == "#" for c in springs[sidx:]):
				return 1
			return 0
		return 0
	run_to_match = runs[ridx]
	cidx = sidx
	o = 0
	# check if next run of # is too big
	while cidx <= len(springs) - run_to_match:
		if springs[cidx] == "#":
			if all(c in "#?" for c in springs[cidx:cidx + run_to_match]) and (cidx + run_to_match == len(springs) or springs[cidx + run_to_match] in ".?"):
				o += recursive_calc(springs, runs, cidx + run_to_match + 1, ridx + 1)
			return o
		if all(c in "#" for c in springs[cidx:cidx + run_to_match]):
			# force break
			try:
				if springs[cidx + run_to_match] == "#":
					return o
			except:
				pass
			o += recursive_calc(springs, runs, cidx + run_to_match + 1, ridx + 1)
			break
		if all(c in "#?" for c in springs[cidx:cidx + run_to_match]) and (cidx + run_to_match == len(springs) or springs[cidx + run_to_match] in ".?") and (cidx == 0 or springs[cidx - 1] in ".?"):
			o += recursive_calc(springs, runs, cidx + run_to_match + 1, ridx + 1)
		cidx += 1
	return o
		



def part2(inp):
	o = 0
	for spring, run in inp:
		o += num_opts2(spring, run)
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
