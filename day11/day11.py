#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#....."""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 374)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 82000210)
		pass

def parse_input(inp, p2mode = False):
	return readin.grid_set_input(inp, "#")

	o = []
	for idx, line in enumerate(inp):
		return line.strip()
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	double_rows = set()
	double_cols = set()
	for y in range(max(i[0] for i in inp)):
		if not any(i[0] == y for i in inp):
			double_rows.add(y)
	for x in range(max(i[1] for i in inp)):
		if not any(i[1] == x for i in inp):
			double_cols.add(x)
	o = 0
	for p in inp:
		for p2 in inp:
			d = 0
			if p == p2:
				continue
			for y in range(min(p[0], p2[0]), max(p[0], p2[0])):
				if y in double_rows:
					d += 2
				else:
					d += 1
				
			for x in range(min(p[1], p2[1]), max(p[1], p2[1])):
				if x in double_cols:
					d += 2
				else:
					d += 1
			o += d
	return o // 2

def part2(inp):
	double_rows = set()
	double_cols = set()
	for y in range(max(i[0] for i in inp)):
		if not any(i[0] == y for i in inp):
			double_rows.add(y)
	for x in range(max(i[1] for i in inp)):
		if not any(i[1] == x for i in inp):
			double_cols.add(x)
	o = 0
	for p in inp:
		for p2 in inp:
			d = 0
			if p == p2:
				continue
			for y in range(min(p[0], p2[0]), max(p[0], p2[0])):
				if y in double_rows:
					d += 1000000
				else:
					d += 1
				
			for x in range(min(p[1], p2[1]), max(p[1], p2[1])):
				if x in double_cols:
					d += 1000000
				else:
					d += 1
			o += d
	return o // 2

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
