#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#...."""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 136)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 64)
		pass

def parse_input(inp, p2mode = False):
	#return readin.grid_input(inp)
	return (readin.grid_set_input(inp, "#"), readin.grid_set_input(inp, "O"))
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	cubes, rounds = inp
	while True:
		any_up = False
		nr = set()
		for r in rounds:
			ry, rx = r
			if ry > 0 and (ry - 1, rx) not in cubes and (ry - 1, rx) not in rounds:
				any_up = True
				nr.add((ry - 1, rx))
			else:
				nr.add(r)
		if any_up:
			rounds = nr
		else:
			break
		pass
	my = max(r[0] for r in cubes)
	o = 0
	for r in rounds:
		o += (my - r[0] + 1)
	return o

def part2(inp):
	cubes, rounds = inp
	cycle = 0
	seen = {}
	mx = max(c[1] for c in cubes)
	my = max(c[0] for c in cubes)
	i = 1
	while True:
		while True:
			any_up = False
			nr = set()
			for r in rounds:
				ry, rx = r
				if cycle == 0:
					if ry > 0 and (ry - 1, rx) not in cubes and (ry - 1, rx) not in rounds:
						any_up = True
						nr.add((ry - 1, rx))
					else:
						nr.add(r)
				elif cycle == 1:
					if rx > 0 and (ry, rx - 1) not in cubes and (ry, rx - 1) not in rounds:
						any_up = True
						nr.add((ry, rx - 1))
					else:
						nr.add(r)
				elif cycle == 2:
					if ry < my and (ry + 1, rx) not in cubes and (ry + 1, rx) not in rounds:
						any_up = True
						nr.add((ry + 1, rx))
					else:
						nr.add(r)
				elif cycle == 3:
					if rx < mx and (ry, rx + 1) not in cubes and (ry, rx + 1) not in rounds:
						any_up = True
						nr.add((ry, rx + 1))
					else:
						nr.add(r)
			rounds = nr
			if not any_up:
				break
			pass
		if cycle < 3:
			cycle += 1
		else:
			if frozenset(rounds) in seen.keys():
				k = frozenset(rounds)
				cycle_len = i - seen[k]
				to_goal = 1000000000 - seen[k]
				leftover = to_goal % cycle_len
				lsk = leftover + seen[k]
				assert 5 in seen.values()
				for k, v in seen.items():
					o = 0
					for r in k:
						o += (my - r[0] + 1)
					if v == lsk:
						have = k
				o = 0
				for r in have:
					o += (my - r[0] + 1)
				return o
			else:
				seen[frozenset(rounds)] = i
			cycle = 0
			i += 1
	my = max(r[0] for r in cubes)
	o = 0
	for r in rounds:
		o += (my - r[0] + 1)
	return o

# not 101669

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
