#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """...........
.....###.#.
.###.##..#.
..#.#...#..
....#.#....
.##..S####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
..........."""
		self.test = parse_input(self.testinput.split("\n"))

		s1 = "." * 131 + "\n"
		s = ""
		s += (s1 * 65)
		s += ("." * 65) + "S" + ("." * 65) + "\n"
		s += (s1 * 64)
		s += ("."  * 131)
		self.testinput2 = """...
.S.
..."""
		self.test2 = parse_input(s.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 42)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 702322399865956)
		pass

def parse_input(inp, p2mode = False):
	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	#pr(inp)
	g = grid.graph_from_grid(inp, lambda p0, p1: 1 if inp[p0[0]][p0[1]] in "S." and inp[p1[0]][p1[1]] in "S." else -1, corners = False)
	for y in range(len(inp)):
		for x in range(len(inp[0])):
			if inp[y][x] == "S":
				start = (y, x)
				break
	#pr(g)
	dist, prev = g.dijkstra(start)
	#pr(dist)
	o = 0
	for k, v in dist.items():
		if v % 2 == 0 and v <= 64:
			o += 1

	return o

def part2(inp):
	my = len(inp)
	mx = len(inp[0])
	assert my == mx
	d = my
	half = d // 2
	assert d % 2 == 1
	g = grid.graph_from_grid(inp, lambda p0, p1: 1 if inp[p0[0]][p0[1]] in "S." and inp[p1[0]][p1[1]] in "S." else -1, corners = False)
	start = (half, half)
	for oth in range(d):
		assert (oth, half) in g.nodes
		assert (half, oth) in g.nodes
	s0 = start[0]
	dist, _ = g.dijkstra(start)
	up = (0, half)
	left = (half, 0)
	right = (half, mx - 1)
	down = (my - 1, half)
	dup, _ = g.dijkstra(up)
	dleft, _ = g.dijkstra(left)
	dright, _ = g.dijkstra(right)
	ddown, _ = g.dijkstra(down)
	ups = len(list(filter(lambda v: v % 2 == 0 and v <= d - 1, dup.values())))
	downs = len(list(filter(lambda v: v % 2 == 0 and v <= d - 1, ddown.values())))
	lefts = len(list(filter(lambda v: v % 2 == 0 and v <= d - 1, dleft.values())))
	rights = len(list(filter(lambda v: v % 2 == 0 and v <= d - 1, dright.values())))


	ul = (0, 0)
	ur = (0, mx - 1)
	dl = (my - 1, 0)
	dr = (my - 1, mx - 1)
	dul, _ = g.dijkstra(ul)
	dur, _ = g.dijkstra(ur)
	ddl, _ = g.dijkstra(dl)
	ddr, _ = g.dijkstra(dr)
	uls = len(list(filter(lambda v: v % 2 == 0 and v <= half - 1, dul.values())))
	urs = len(list(filter(lambda v: v % 2 == 0 and v <= half - 1, dur.values())))
	dls = len(list(filter(lambda v: v % 2 == 0 and v <= half - 1, ddl.values())))
	drs = len(list(filter(lambda v: v % 2 == 0 and v <= half - 1, ddr.values())))
	evens = len(list(filter(lambda v: v % 2 == 0, dist.values())))
	odds = len(list(filter(lambda v: v % 2 == 1, dist.values())))
	assert evens + odds == len(dist.values())
	steps = 26501365
	l = 0
	for i in range(steps):
		l += 2 * (i + 1)
	l += (steps + 1)
	substeps = (steps - half - d)
	o = (substeps // 2)
	o = 0
	plots = 1
	while steps > 0:
		assert (steps - half) % d == 0
		if steps % 2 == 1 and steps >= d * 2:
			o += (plots * odds)
		elif steps % 2 == 0 and steps >= d * 2:
			o += (plots * evens)
		elif steps % 2 == 1 and steps >= d:
			assert False
		elif steps % 2 == 0:
			assert steps == d + half
			nplots = plots + 4 if plots != 1 else 4
			sideplots = plots // 4
			nsideplots = nplots // 4
			o += (plots * evens)
			o += ups + downs + lefts + rights
			ul = 0
			dl = 0
			ur = 0
			dr = 0
			
			for k in dist.keys():
				up_reachable = dup[k] % 2 == 0 and dup[k] <= d - 1
				left_reachable = dleft[k] % 2 == 0 and dleft[k] <= d - 1
				right_reachable = dright[k] % 2 == 0 and dright[k] <= d - 1
				down_reachable = ddown[k] % 2 == 0 and ddown[k] <= d - 1
				if up_reachable or left_reachable:
					o += sideplots
					ul += 1
				if left_reachable or down_reachable:
					o += sideplots
					dl += 1
				if down_reachable or right_reachable:
					o += sideplots
					dr += 1
				if right_reachable or up_reachable:
					o += sideplots
					ur += 1
			steps = 0
			o += (uls + urs + dls + drs) * nsideplots
		steps -= d
		if plots == 1:
			plots = 4
		else:
			plots += 4
	assert o != 609584457076984
	return o

# not 613470126643295
# not 613475472012638
# not 613472321190079
# not 609584457076984

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
