#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 4361)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 467835)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		o.append(list(c for c in line.strip()))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def get(g, y, x):
	try:
		return g[y][x]
	except:
		return " "

def part1(inp):
	y = 0
	symbols = "!@#$%^&*()_-+=/" 
	o = 0
	while y < len(inp):
		x = 0
		while x < len(inp[0]):
			if inp[y][x] in "0123456789":
				l = 1
				while True:
					x += 1
					if x < len(inp[0]) and inp[y][x] in "0123456789":
						l += 1
					else:
						x -= 1
						break
				if (y > 0 and any(get(inp, y - 1, x1) in symbols for x1 in range(x - l, x + 2))) or (y < len(inp) - 1 and any(get(inp, y + 1, x1) in symbols for x1 in range(x - l, x + 2))) or get(inp, y, x - l) in symbols or get(inp, y, x + 1) in symbols:
					n = "".join(inp[y][x1] for x1 in range(x - l + 1, x + 1))
					o += int(n)
				else:
					pass#n = "".join(inp[y][x1] for x1 in range(x - l + 1, x + 1))
			x += 1
		y += 1
	return o

def part2(inp):
	y = 0
	symbols = "*" 
	o = 0
	d = Counter()
	d2 = defaultdict(list)
	while y < len(inp):
		x = 0
		while x < len(inp[0]):
			if inp[y][x] in "0123456789":
				l = 1
				while True:
					x += 1
					if x < len(inp[0]) and inp[y][x] in "0123456789":
						l += 1
					else:
						x -= 1
						break
				poses = []
				n = "".join(inp[y][x1] for x1 in range(x - l + 1, x + 1))
				for x1 in range(max(0, x - l), min(len(inp[0]), x + 2)):
					if y > 0:
						poses.append((y - 1, x1))
					if y < len(inp):
						poses.append((y + 1, x1))
				if x - l >= 0:
					poses.append((y, x - l))
				if x + 1 < len(inp[0]):
					poses.append((y, x + 1))
				for pos in poses:
					if get(inp, pos[0], pos[1]) == "*":
						d[pos] += 1
						d2[pos].append(int(n))
			x += 1
		y += 1
	for k, v in d.items():
		if v == 2:
			o += d2[k][0] * d2[k][1]
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
