#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """111111111111
999999999991
999999999991
999999999991
999999999991"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 102)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 71)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		o.append(list(int(c) for c in line.strip()))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	g = tuple(tuple(l) for l in inp)
	nodes = []
	m = len(g)
	edges = defaultdict(list)
	for y1 in range(len(g)):
		for x1 in range(len(g[y1])):
			for d in (direc.LEFT, direc.RIGHT, direc.UP, direc.DOWN):
				for n in range(4):
					node = (y1, x1, d, n)
					nodes.append(node)
					if n < 3:
						if 0 <= y1 + d[0] <= m - 1 and 0 <= x1 + d[1] <= m - 1:
							edges[node].append(graph.Edge((y1 + d[0], x1 + d[1], d, n + 1), g[y1 + d[0]][x1 + d[1]]))
					ld = direc.turn_left(d)
					rd = direc.turn_right(d)
					if 0 <= y1 + rd[0] <= m - 1 and 0 <= x1 + rd[1] <= m - 1:
						edges[node].append(graph.Edge((y1 + rd[0], x1 + rd[1], rd, 1), g[y1 + rd[0]][x1 + rd[1]]))
					if 0 <= y1 + ld[0] <= m - 1 and 0 <= x1 + ld[1] <= m - 1:
						edges[node].append(graph.Edge((y1 + ld[0], x1 + ld[1], ld, 1), g[y1 + ld[0]][x1 + ld[1]]))
	g = graph.Graph(nodes, edges)
	dist, prev = g.dijkstra((0, 0, direc.RIGHT, 0))
	b = math.inf
	for k, v in dist.items():
		if k[0] == m - 1 and k[1] == m - 1:
			b = min(b, v)
	return b

def part2(inp):
	g = tuple(tuple(l) for l in inp)
	nodes = []
	my = len(g)
	mx = len(g[0])
	edges = defaultdict(list)
	for y1 in range(len(g)):
		for x1 in range(len(g[y1])):
			for d in (direc.LEFT, direc.RIGHT, direc.UP, direc.DOWN):
				for n in range(11):
					node = (y1, x1, d, n)
					nodes.append(node)
					if n < 10:
						if 0 <= y1 + d[0] <= my - 1 and 0 <= x1 + d[1] <= mx - 1:
							edges[node].append(graph.Edge((y1 + d[0], x1 + d[1], d, n + 1), g[y1 + d[0]][x1 + d[1]]))
					ld = direc.turn_left(d)
					rd = direc.turn_right(d)
					if n >= 4:
						if 0 <= y1 + rd[0] <= my - 1 and 0 <= x1 + rd[1] <= mx - 1:
							edges[node].append(graph.Edge((y1 + rd[0], x1 + rd[1], rd, 1), g[y1 + rd[0]][x1 + rd[1]]))
						if 0 <= y1 + ld[0] <= my - 1 and 0 <= x1 + ld[1] <= mx - 1:
							edges[node].append(graph.Edge((y1 + ld[0], x1 + ld[1], ld, 1), g[y1 + ld[0]][x1 + ld[1]]))
	g = graph.Graph(nodes, edges)
	dist, _ = g.dijkstra((0, 0, direc.RIGHT, 0))
	dist2, _ = g.dijkstra((0, 0, direc.DOWN, 0))
	b = math.inf
	for k, v in dist.items():
		if k[0] == my - 1 and k[1] == mx - 1 and k[-1] >= 4:
			b = min(b, v)
	for k, v in dist2.items():
		if k[0] == my - 1 and k[1] == mx - 1 and k[-1] >= 4:
			b = min(b, v)
	return b

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
