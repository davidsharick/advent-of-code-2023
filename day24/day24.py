#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
#from z3 import *
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test, (7, 27)), 0)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		pos, vel = line.strip().split("@")
		pos2 = tuple(int(c.strip().strip(",")) for c in pos.split())
		vel2 = tuple(int(c.strip().strip(",")) for c in vel.split())
		o.append((pos2, vel2))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp, r):
	o = 0
	for h in inp:
		for h2 in inp:
			if h == h2:
				continue
			p = intersect(h, h2)
			if p is not None:
				if p[0] >= r[0] and p[0] <= r[1] and p[1] >= r[0] and p[1] <= r[1]:
					if future(p, h, h2):
						o += 1

	return o // 2


def intersect(h1, h2):
	p1, v1 = h1
	p2, v2 = h2
	if parallel(v1, v2):
		return None
	c1 = (v1[0] * p1[1] - v1[1] * p1[0])
	c2 = (v2[0] * p2[1] - v2[1] * p2[0])
	b1 = -v1[0]
	b2 = -v2[0]
	a1 = v1[1]
	a2 = v2[1]
	xi = (b1 * c2 - b2 * c1) / (a1 * b2 - a2 * b1)
	yi = (a2 * c1 - a1 * c2) / (a1 * b2 - a2 * b1)
	return (xi, yi)

def intersect2(h1, h2):
	p1, v1 = h1
	p2, v2 = h2
	if parallel2(v1, v2):
		return None
	c1 = (v1[0] * p1[1] - v1[1] * p1[0])
	c2 = (v2[0] * p2[1] - v2[1] * p2[0])
	b1 = -v1[0]
	b2 = -v2[0]
	a1 = v1[1]
	a2 = v2[1]
	xi = (b1 * c2 - b2 * c1) / (a1 * b2 - a2 * b1)
	yi = (a2 * c1 - a1 * c2) / (a1 * b2 - a2 * b1)

	
	return (xi, yi)

def future(p, h1, h2):
	good = True
	for d in range(2):
		for h in [h1, h2]:
			if (h[1][d] > 0 and p[d] > h[0][d]) or (h[1][d] < 0 and p[d] < h[0][d]) or (h[1][d] == 0 and p[d] == h[0][d]):
				pass
			else:
				return False
	return good

def parallel(v1, v2):
	ratio = v1[0] / v2[0]
	if v2[1] * ratio == v1[1]:# and v2[2] * ratio == v1[2]:
		return True
	return False

def parallel2(v1, v2):
	ratio = v1[0] / v2[0]
	if v2[1] * ratio == v1[1] and v2[2] * ratio == v1[2]:
		return True
	return False

def part2(inp):
	invalid = defaultdict(set)
	for h in inp:
		for h2 in inp:
			if h == h2:
				continue
			for d in range(3):
				if h[0][d] > h2[0][d] and h[1][d] > h2[1][d]:
					for i in range(h2[1][d], h[1][d]):
						invalid[d].add(i)
	# drawn from https://old.reddit.com/r/adventofcode/comments/18pptor/2023_day_24_part_2java_is_there_a_trick_for_this/keps780/
	# preprocessing from https://old.reddit.com/r/adventofcode/comments/18pptor/2023_day_24_part_2java_is_there_a_trick_for_this/kepxbew/
	for x in range(-500, 501):
		if x in invalid[0]:
			continue
		for y in range(-500, 501):
			if y in invalid[1]:
				continue
			for z in range(-500, 501):
				if z in invalid[2]:
					continue
				nh = []
				for h in inp:
					nh.append((h[0], (h[1][0] - x, h[1][1] - y, h[1][2] - z)))
				good = True
				for hs in nh:
					for hs2 in nh[1:]:
						try:
							p = intersect2(hs, hs2)
						except:
							continue
						if p is not None and future(p, hs, hs2):
							if p[0] % 1 == 0 and p[1] % 1 == 0:
								t0 = (p[0] - hs[0][0]) // hs[1][0]
								t1 = (p[0] - hs2[0][0]) // hs2[1][0]
								assert t0 % 1 == 0 and t1 % 1 == 0
								z0 = hs[0][2] + (hs[1][2] * t0)
								z1 = hs2[0][2] + (hs2[1][2] * t1)
								if z0 == z1:
									goal = (p[0], p[1], z0)
									good = True
									for h3 in nh[2:]:
										# does h3 hit (p[0], p[1], z0) at some t?
										for d in range(3):
											if int(goal[d]) == h3[0][d] and h3[1][d] == 0:
												continue
											if (goal[d] - h3[0][d]) % h3[1][d] != 0:
												good = False
									if good:
										return int(sum(p) + z0)
						break
					break
	return 0
	slv = Solver()
	x, y, z, u, v, w, r, s, t = Ints("x y z u v w r s t")
	a1 = inp[0][0][0]
	a2 = inp[1][0][0]
	a3 = inp[2][0][0]
	c1 = inp[0][0][1]
	c2 = inp[1][0][1]
	c3 = inp[2][0][1]
	e1 = inp[0][0][2]
	e2 = inp[1][0][2]
	e3 = inp[2][0][2]
	b1 = inp[0][1][0]
	b2 = inp[1][1][0]
	b3 = inp[2][1][0]
	d1 = inp[0][1][1]
	d2 = inp[1][1][1]
	d3 = inp[2][1][1]
	f1 = inp[0][1][2]
	f2 = inp[1][1][2]
	f3 = inp[2][1][2]
	pr(simplify(And(x - a1 == r*b1 - r*u, y - c1 == r*d1 - r*v, z - e1 == r*f1 - r*w, x - a2 == s*b2 - s*u, y - c2 == s*d2 - s*v, z - e2 == s*f2 - s*w, x - a3 == t*b3 - t*u, y - c3 == t*d3 - t*v, z - e3 == t*f3 - t*w)))
	pr(slv.check())
	return eval(str(slv.model()[x] + slv.model()[y] + slv.model()[z]))

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i, (200000000000000, 400000000000000)))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
