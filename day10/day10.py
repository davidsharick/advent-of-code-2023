#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """..F7.
.FJ|.
SJ.L7
|F--J
LJ..."""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """..........
.S------7.
.|F----7|.
.||OOOO||.
.||OOOO||.
.|L-7F-J|.
.|II||II|.
.L--JL--J.
.........."""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 8)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 10)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		o.append(list(line.strip()))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	start = None
	for y, l in enumerate(inp):
		for x, c in enumerate(l):
			if c == "S":
				start = (y, x)



	sy, sx = start
	conn_down = inp[sy + 1][sx] in "|LJ"
	conn_up = inp[sy - 1][sx] in "|7F"
	conn_left = inp[sy][sx - 1] in "-LF"
	conn_right = inp[sy][sx + 1] in "-7J"
	if conn_down and conn_up:
		inp[sy][sx] = "|"
	elif conn_left and conn_right:
		inp[sy][sx] = "-"
	elif conn_left and conn_down:
		inp[sy][sx] = "7"
	elif conn_left and conn_up:
		inp[sy][sx] = "J"
	elif conn_right and conn_up:
		inp[sy][sx] = "L"
	elif conn_down and conn_right:
		inp[sy][sx] = "F"



	def pf(p1, p2):
		py, px = p1
		if inp[py][px] == "|" and abs(p1[0] - p2[0]) == 1:
			return 1
		elif inp[py][px] == "-" and abs(p1[1] - p2[1]) == 1:
			return 1
		elif inp[py][px] == "L" and (p2[0] == p1[0] - 1 or p2[1] == p1[1] + 1):
			return 1
		elif inp[py][px] == "J" and (p2[0] == p1[0] - 1 or p2[1] == p1[1] - 1):
			return 1
		elif inp[py][px] == "F" and (p2[0] == p1[0] + 1 or p2[1] == p1[1] + 1):
			return 1
		elif inp[py][px] == "7" and (p2[0] == p1[0] + 1 or p2[1] == p1[1] - 1):
			return 1
		return -1

	g = grid.graph_from_grid(inp, pf, corners = False, cull_lone = False)
	dist, prev = g.dijkstra(start)
	return max(0 if p == math.inf else p for p in dist.values())
	pos = start
	while True:
		py, px = pos
		if inp[py][px] == "|":
			pos = (py - 1, px)
		elif inp[py][px] == "-":
			pos = (py, px + 1)
	return 0

def part2(inp):
	start = None
	for y, l in enumerate(inp):
		for x, c in enumerate(l):
			if c == "S":
				start = (y, x)



	sy, sx = start
	conn_down = inp[sy + 1][sx] in "|LJ"
	conn_up = inp[sy - 1][sx] in "|7F"
	conn_left = inp[sy][sx - 1] in "-LF"
	conn_right = inp[sy][sx + 1] in "-7J"
	if conn_down and conn_up:
		inp[sy][sx] = "|"
	elif conn_left and conn_right:
		inp[sy][sx] = "-"
	elif conn_left and conn_down:
		inp[sy][sx] = "7"
	elif conn_left and conn_up:
		inp[sy][sx] = "J"
	elif conn_right and conn_up:
		inp[sy][sx] = "L"
	elif conn_down and conn_right:
		inp[sy][sx] = "F"





	def pf(p1, p2):
		py, px = p1
		if inp[py][px] == "|" and abs(p1[0] - p2[0]) == 1:
			return 1
		elif inp[py][px] == "-" and abs(p1[1] - p2[1]) == 1:
			return 1
		elif inp[py][px] == "L" and (p2[0] == p1[0] - 1 or p2[1] == p1[1] + 1):
			return 1
		elif inp[py][px] == "J" and (p2[0] == p1[0] - 1 or p2[1] == p1[1] - 1):
			return 1
		elif inp[py][px] == "F" and (p2[0] == p1[0] + 1 or p2[1] == p1[1] + 1):
			return 1
		elif inp[py][px] == "7" and (p2[0] == p1[0] + 1 or p2[1] == p1[1] - 1):
			return 1
		return -1

	g = grid.graph_from_grid(inp, pf, corners = False, cull_lone = False)
	dist, prev = g.dijkstra(start)
	loop = set(filter(lambda p: dist[p] != math.inf and dist[p] != None, g.nodes))
	not_in = set()
	queue = []
	for y in range(len(inp)):
		if (y, 0) not in loop:
			not_in.add((y, 0))
			queue.append((y, 0))
		if (y, len(inp[0]) - 1) not in loop:
			not_in.add((y, len(inp[0]) - 1))
			queue.append((y, len(inp[0]) - 1))
	for x in range(len(inp[0])):
		if (0, x) not in loop:
			not_in.add((0, x))
			queue.append((0, x))
		if (len(inp) - 1, x) not in loop:
			not_in.add((len(inp) - 1, x))
			queue.append((len(inp) - 1, x))
	while len(queue) > 0:
		nxt = queue.pop()
		for nh in grid.grid_neighbors(nxt, (0, len(inp) - 1), (0, len(inp[0]) - 1), corners = True):
			if nh in loop or nh in not_in:
				pass
			else:
				queue.append(nh)
				not_in.add(nh)

	"""for y in range(len(inp)):
		s = ""
		for x in range(len(inp[0])):
			if (y, x) in not_in:
				s += "O"
			elif (y, x) in loop:
				s += inp[y][x]
			else:
				if escapable((y, x), not_in, inp, loop):
					s += "O"
				else:
					s += "I"
		print(s)"""
	
	o = 0
	for y in range(len(inp)):
		for x in range(len(inp[0])):
			pos = (y, x)
			if pos in loop or pos in not_in:
				continue
			if escapable(pos, not_in, inp, loop):
				continue
			o += 1
	return o

def escapable(p, outside, grid, loop):
	py, px = p
	starts = [(py - 1, px - 1), (py, px - 1), (py, px), (py - 1, px)] # represents the bottom right corner 
	seen = set()
	queue = starts
	while len(queue) > 0:
		curr = queue.pop()
		if curr in seen:
			continue
		else:
			seen.add(curr)
		if curr in outside:
			return True
		cy, cx = curr
		if cy < 0 or cy >= len(grid) or cx < 0 or cx > len(grid[0]):
			continue
		
		# try down
		try:
			down = (cy + 1, cx)
			if (cy + 1, cx) not in loop or (cy + 1, cx + 1) not in loop:
				queue.append(down)
			elif grid[cy + 1][cx] + grid[cy + 1][cx + 1] in ("|F", "|L", "JL", "7L", "JF", "7F", "||", "7.", "J.", "|.", ".F", ".L", ".|", "J|", "7|"):
				queue.append(down)
		except:
			pass
		
		# try up
		try:
			up = (cy - 1, cx)
			if (cy, cx) not in loop or (cy, cx + 1) not in loop:
				queue.append(up)
			elif grid[cy][cx] + grid[cy][cx + 1] in ("|F", "|L", "JL", "7L", "JF", "7F", "||", "7.", "J.", "|.", ".F", ".L", ".|", "J|", "7|"):
				queue.append(up)
		except:
			pass
		
		# try left
		try:
			left = (cy, cx - 1)
			if (cy, cx) not in loop or (cy + 1, cx) not in loop:
				queue.append(left)
			elif grid[cy][cx] + grid[cy + 1][cx] in ("--", "LF", "L7", "JF", "J7", "L-", "J-", "-.", ".-", "L.", "J.", ".F", ".7", "-F", "-7"):
				queue.append(left)
		except:
			pass
		
		# try right
		try:
			right = (cy, cx + 1)
			if (cy, cx + 1) not in loop or (cy + 1, cx + 1) not in loop:
				queue.append(right)
			elif grid[cy][cx + 1] + grid[cy + 1][cx + 1] in ("--", "LF", "L7", "JF", "J7", "L-", "J-", "-.", ".-", "L.", "J.", ".F", ".7", "-F", "-7"):
				queue.append(right)
		except:
			pass
		
	return False
		


def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
