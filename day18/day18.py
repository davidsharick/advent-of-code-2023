#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 62)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 952408144115)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		tokens = line.strip().split()
		o.append((tokens[0], int(tokens[1]), tokens[2][2:-1]))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	p = (0, 0)
	dug = set()
	dug.add(p)
	for m in inp:
		d, amt, _ = m
		if d == "R":
			d = direc.RIGHT
		elif d == "L":
			d = direc.LEFT
		elif d == "U":
			d = direc.UP
		else:
			d = direc.DOWN
		for i in range(amt):
			np = (p[0] + d[0], p[1] + d[1])
			dug.add(np)
			p = np
	miny = min(p[0] for p in dug)
	minx = min(p[1] for p in dug)
	maxy = max(p[0] for p in dug)
	maxx = max(p[1] for p in dug)
	q = []
	for y in range(miny, maxy + 1):
		if (y, minx) not in dug:
			q.append((y, minx))
		if (y, maxx) not in dug:
			q.append((y, maxx))
	for x in range(minx, maxx + 1):
		if (miny, x) not in dug:
			q.append((miny, x))
		if (maxy, x) not in dug:
			q.append((maxy, x))

	out = set()
	while len(q) > 0:
		nxt = q.pop()
		out.add(nxt)
		for nh in grid.grid_neighbors(nxt, (miny, maxy), (minx, maxx), corners = False):
			if nh in dug or nh[0] < miny or nh[0] > maxy or nh[1] < minx or nh[1] > maxx or nh in out:
				pass
			else:
				q.append(nh)
				out.add(nh)
	o = 0
	for y in range(miny, maxy + 1):
		for x in range(minx, maxx + 1):
			if (y, x) not in dug and (y, x) not in out:
				o += 1
	return o + len(dug)

# below 88007131464365
# below 88007104020978
# above 88007101897393
# above 88007069108853

def part2(inp):
	p = (0, 0)
	minx = 0
	maxx = 0
	miny = 0
	maxy = 0
	lines = []
	for m in inp:
		d, amt, c = m
		d = "RDLU"[int(c[-1])]
		amt = int(c[:-1], 16)
		if d == "R":
			d = direc.RIGHT
		elif d == "L":
			d = direc.LEFT
		elif d == "U":
			d = direc.UP
		else:
			d = direc.DOWN
		np = (p[0] + d[0] * amt, p[1] + d[1] * amt)
		miny = min(miny, np[0])
		maxy = max(maxy, np[0])
		minx = min(minx, np[1])
		maxx = max(maxx, np[1])
		lines.append((p, np, d))
		p = np
	o = 0
	# y-batching
	curr = miny + 1
	batches = [(miny, miny)]
	lineys = []
	for l in lines:
		lineys.append(l[0][0])
		lineys.append(l[1][0])
		lineys.append(l[0][0] + 1)
		lineys.append(l[1][0] + 1)
	lineys.append(maxy)
	lineys = list(set(lineys))
	lineys.sort()
	for y in lineys[1:]:
		batches.append((curr, y - 1))
		curr = y
	for batch in batches:
		if batch[0] > batch[1]:
			continue
		y = batch[0]
		# find all intersections with the lines
		crosses = []
		maybes_opp = []
		maybes_paired = []
		paired_dict = {}
		for idx, line in enumerate(lines):
			p0, p1, d = line
			if d in (direc.LEFT, direc.RIGHT) and p0[0] == y:
				if lines[idx - 1][-1] != lines[idx + 1][-1]:
					# stay inside/outside
					#if len(crosses) % 2 == 0: # currently outside, so we go inside; else we stay inside and don't care
					p = (min(p0[1], p1[1]), max(p0[1], p1[1]))
					paired_dict[p[0]] = p
					maybes_paired.append(p[0])
				else:
					# flip inside/outside
					# are we inside?
					#if len(crosses) % 2 != 0: # no
					#	crosses.append(min(p0[1], p1[1]))
					#else: # yes
					p = (min(p0[1], p1[1]), max(p0[1], p1[1]))
					maybes_opp.append(min(p0[1], p1[1]))
					paired_dict[p[0]] = p
			if d in (direc.UP, direc.DOWN) and y > min(p0[0], p1[0]) and y < max(p0[0], p1[0]):
				crosses.append(p0[1])
			elif d in (direc.UP, direc.DOWN) and (y == p0[0] or y == p1[0]):
				pass
		maybes = []
		#maybes.extend(maybes_even)
		maybes.extend(maybes_opp)
		maybes.extend(maybes_paired)
		maybes.sort()
		for m in maybes:
			if len(list(filter(lambda c: c < m, crosses))) % 2 == 0:
				if m in maybes_opp:
					crosses.append(paired_dict[m][0])
				elif m in maybes_paired:
					for c in paired_dict[m]:
						crosses.append(c)
			else:
				if m in maybes_opp:
					crosses.append(paired_dict[m][1])
		crosses.sort()
		crosses.sort()
		# find number within
		x = minx
		assert len(crosses) % 2 == 0
		d = 0
		idx = 0
		while idx < len(crosses):
			d += (crosses[idx + 1]) - crosses[idx] + 1
			idx += 2
		o += (d * (batch[1] - batch[0] + 1))
	for l in line:
		if l[-1] in (direc.LEFT, direc.DOWN):
			assert (l[0][0], l[0][0]) in batches
			assert any(b[0] == l[0][0] + 1 for b in batches)
			assert any(b[1] == l[0][0] - 1 for b in batches)
	return o


def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
