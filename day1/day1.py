#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 142)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 281)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		o.append(list(c for c in line.strip()))
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	o = 0
	cr = "0123456789"
	for l in inp:
		c = ""
		for idx in range(len(l)):
			if l[idx] in cr:
				c += l[idx]
				break
		for idx in range(len(l) - 1, -1, -1):
			if l[idx] in cr:
				c += l[idx]
				break
		o += int(c)
	return o


def part2(inp):
	o = 0
	cr = "0123456789"
	nums = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
	for l in inp:
		c = ""
		total = "".join(c for c in l)
		m = 188890
		b = -1
		for idx in range(len(l)):
			if l[idx] in cr:
				m = idx
				b = l[idx]
				break
		for nidx, num in enumerate(nums):
			if num in total:
				idx2 = total.find(num)
				if idx2 < m:
					b = nidx
					m = idx2
		c += str(b)
		m = 0
		b = -1
		for idx in range(len(l) - 1, -1, -1):
			if l[idx] in cr:
				m = idx
				b = l[idx]
				break
		for nidx, num in enumerate(nums):
			if num in total:
				idx2 = total.rfind(num)
				if idx2 > m:
					b = nidx
					m = idx2
		c += str(b)
		o += int(c)
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
