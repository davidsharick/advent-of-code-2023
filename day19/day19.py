#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 19114)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 167409079868000)
		pass

def parse_input(inp, p2mode = False):

	rules = {}
	parts = []
	in_parts = False
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			in_parts = True
		else:
			if in_parts:
				l = line.strip()[1:-1]
				p = {}
				for t in l.split(","):
					t2 = t.split("=")
					p[t2[0]] = int(t2[1])
				parts.append(p)
			else:
				t = line.strip().split("{")
				rn = t[0]
				rule = t[1][:-1].split(",")
				rules[rn] = rule
	return rules, parts

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	rules, parts = inp
	#pr(rules)
	#pr(parts)
	o = 0
	for p in parts:
		#pr(p)
		if is_acc(rules, p):
			o += sum(p.values())
	return o

def is_acc(rules, p):
	curr_rule = "in"
	while True:
		r = rules[curr_rule]
		for p2 in r:
			if p2 == "R":
				return False
			elif p2 == "A":
				return True
			else:
				if ":" not in p2:
					curr_rule = p2
					break
				rule, dest = p2.split(":")
				if matches(p, rule):

					if dest == "R":
						return False
					elif dest == "A":
						return True
					else:
						curr_rule = dest
						break

def matches(p, rule):
	sign = "<" if "<" in rule else ">"
	var, target = rule.split(sign)
	if sign == "<":
		if p[var] < int(target):
			return True
		else:
			return False
	elif sign == ">":
		if p[var] > int(target):
			return True
		else:
			return False

rules = {}

def part2(inp):
	global rules
	rules, _ = inp
	"""nodes = list(rules.keys())
	nodes.append("R")
	nodes.append("A")
	start = "in"
	edges = defaultdict(list)
	for label, rule_parts in rules.items():
		for rule_part in rule_parts:
			if ":" in rule_part:
				edges[label].append(graph.Edge(rule_part.split(":")[1]))
			else:
				edges[label].append(graph.Edge(rule_part))
	pr(edges)
	g = graph.Graph(nodes, edges)"""
	start = "in"
	all_ranges = ((1, 4000), (1, 4000), (1, 4000), (1, 4000))
	a = recursive(all_ranges, start)
	o = 0
	for range_set in a:
		prod = 1
		for var in range_set:
			length = var[1] - var[0] + 1
			prod *= length
		o += prod
	return o

conv = {"x": 0, "m": 1, "a": 2, "s": 3}

@lru_cache(None)
def recursive(ranges, node):
	end_ranges = []
	if node == "R":
		return []
	elif node == "A":
		return [ranges]
	for rule_part in rules[node]:
		if ":" in rule_part:
			cond, dest = rule_part.split(":")
			sign = "<" if "<" in cond else ">"
			var, target = cond.split(sign)
			target = int(target)
			r = ranges[conv[var]]
			if sign == "<":
				if r[0] >= target:
					# this rule is unfollowable
					continue
				# the follow case
				nr = list(ranges)
				nr[conv[var]] = (r[0], min(r[1], target - 1))
				end_ranges.extend(recursive(tuple(nr), dest))

				# modify curr ranges too
				nr2 = list(ranges)
				nr2[conv[var]] = (max(r[0], target), r[1])
				ranges = tuple(nr2)
			elif sign == ">":
				if r[1] <= target:
					# this rule is unfollowable
					continue
				nr = list(ranges)
				nr[conv[var]] = (max(r[0], target + 1), r[1])
				end_ranges.extend(recursive(tuple(nr), dest))

				nr2 = list(ranges)
				nr2[conv[var]] = (r[0], min(r[1], target))
				ranges = tuple(nr2)
		else:
			assert ":" not in rule_part
			end_ranges.extend(recursive(tuple(ranges), rule_part))
	return end_ranges 

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
