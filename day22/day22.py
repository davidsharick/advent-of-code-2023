#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import copy
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 5)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		bricks = line.strip().split("~")
		b = tuple(tuple(int(c) for c in br.split(",")) for br in bricks)
		o.append(b)
	return o

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	bricks = inp
	bricks.reverse()
	drops = 0
	settled = set()
	attach = defaultdict(list)
	d = Counter()
	for b in bricks:
		p0, p1 = b
		d[(p1[0] - p0[0], p1[1] - p0[1], p1[2] - p0[2])] += 1
	ideal_y = 1
	while True:
		i2 = 0
		fallen = False
		best = (0, None)
		droppable = []
		for b in bricks:
			p0, p1 = b
			base = (p0, (p1[0], p1[1], p0[2]))
			if p0[2] <= ideal_y:
				continue
			if b in settled:
				continue
			max_drop = b[0][2] - 1
			for b2 in bricks:
				if b == b2:
					continue
				t0, t1 = b2
				top = ((t0[0], t0[1], t1[2]), t1)
				if t1[2] < p0[2]:
					if twod_overlap(base, top):
						if (p0[2] - t1[2]) - 1 < max_drop:
							max_drop = min(max_drop, (p0[2] - t1[2]) - 1)
						if max_drop == 0:
							break
			new_y = p0[2] - max_drop
			if new_y == ideal_y:
				droppable.append(b)
		if len(droppable) == 0:
			ideal_y += 1
			if ideal_y >= 350:
				break
			continue
		for d in droppable:
			p0, p1 = d
			nd = ((p0[0], p0[1], ideal_y), (p1[0], p1[1], ideal_y + (p1[2] - p0[2])))
			bricks.remove(d)
			bricks.append(nd)
			settled.add(nd)
	obricks = copy.deepcopy(bricks)
	o = 0
	for b in bricks:
		if not will_fall(copy.deepcopy(obricks), b):
			o += 1
	return o, bricks

def will_fall(bricks, ignore):
	bricks.remove(ignore)
	for i in range(len(bricks)):
		b = bricks[i]
		can_fall = True
		p0, p1 = b
		base = (p0, (p1[0], p1[1], p0[2]))
		assert p0[2] > 0 and p1[2] > 0
		if p0[2] == 1:
			continue
		for b2 in bricks:
			t0, t1 = b2
			top = ((t0[0], t0[1], t1[2]), t1)
			if t1[2] == p0[2] - 1:
				if twod_overlap(base, top):
					can_fall = False
					break
		if can_fall:
			return True
	return False

def twod_overlap(p0, p1):
	range_0 = ((p0[0][0], p0[0][1]), (p0[1][0], p0[1][1]))
	range_1 = ((p1[0][0], p1[0][1]), (p1[1][0], p1[1][1]))
	x_overlap = (max(range_0[0][0], range_1[0][0]), min(range_0[1][0], range_1[1][0]))
	y_overlap = (max(range_0[0][1], range_1[0][1]), min(range_0[1][1], range_1[1][1]))
	return x_overlap[0] <= x_overlap[1] and y_overlap[0] <= y_overlap[1]


def part2(inp, bricks):
	#bricks = [((0, 0, 2), (2, 0, 2)), ((1, 0, 1), (1, 2, 1)), ((0, 2, 2), (2, 2, 2)), ((2, 0, 3), (2, 2, 3)), ((0, 0, 3), (0, 2, 3)), ((0, 1, 4), (2, 1, 4)), ((1, 1, 5), (1, 1, 6))]
	supports = defaultdict(list)
	supported = defaultdict(list)
	for b in bricks:
		p0, p1 = b
		base = (p0, (p1[0], p1[1], p0[2]))
		#if p0[2] == 1:
		#	continue
		for b2 in bricks:
			if b == b2:
				continue
			t0, t1 = b2
			top = ((t0[0], t0[1], t1[2]), t1)
			if t1[2] == p0[2] - 1:
				if twod_overlap(base, top):
					supports[b2].append(b)
					supported[b].append(b2)
	o = 0
	for b in bricks:
		fallen = set()
		fallen.add(b)
		while True:
			x = len(fallen)
			for b2 in bricks:
				if b2 in fallen:
					continue
				if all(b3 in fallen for b3 in supported[b2]) and len(supported[b2]) > 0:
					fallen.add(b2)
			if len(fallen) == x:
				o += x - 1
				break

	return o

# below 684568
# below 75379

def recursive(tree, b):
	o = 0
	for b2 in tree[b]:
		o += 1 + recursive(tree, b2)
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	p1, bricks = part1(i)
	print(p1)
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i, bricks))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
