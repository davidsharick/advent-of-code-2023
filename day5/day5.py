#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 0)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	maps = {}
	seeds = []
	to = None
	for idx, line in enumerate(inp):
		if "seeds" in line:
			seeds = list(int(t) for t in line.strip().split()[1:])
		elif "map" in line:
			if to is not None:
				maps[frm] = (to, cm)
			t = line.strip().split()[0]
			t2 = t.split("-")
			frm, to = t2[0], t2[2]
			cm = []
		elif len(line.strip()) > 0:
			tokens = line.strip().split()
			cm.append(tuple(int(c) for c in tokens))
	maps[frm] = (to, cm)
	return seeds, maps

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	seeds, maps = inp
	b = math.inf
	for s in seeds:
		b = min(b, conv(s, maps))
	return b

def conv(seed, maps):
	have = "seed"
	curr = seed
	while have != "location":
		phave = have
		nxt, convs = maps[have]
		for conv in convs:
			if curr >= conv[1] and curr <= conv[1] + conv[2]:
				curr -= conv[1]
				curr += conv[0]
				have = nxt
				#pr(have)
				break
		if have == phave:
			curr = curr
			have = nxt
	return curr

def part2(inp):
	maps = inp[1]
	for k, v in maps.items():
		if v[0] == "location":
			return min(m[0] for m in v[1]) # bruh

	"""seeds, maps = inp
	maps2 = {}
	for k, v in maps.items():
		rn = v[1]
		r2 = []
		for v2 in rn:
			nv = (v2[1], v2[1] + v2[2] - 1, v2[0] - v2[1])
			r2.append(nv)
		maps2[k] = (v[0], r2)
	pr(maps2)"""
		
	"""rs = maps["humidity"]
	r2 = []
	for r in rs[1]:
		
		r2.append((r[1], r[1] + r[2] - 1))
	curr_ranges = []
	for r in r2:
		new_ranges = []
		any_combos = False
		for r2 in curr_ranges:
			if ranges.one_dimension_overlap(r, r2):
				new_range = (min(min(r), min(r2)), max(max(r), max(r2)))
				new_ranges.append(new_range)
				any_combos = True
			else:
				new_ranges.append(r2)
		if not any_combos:
			new_ranges.append(r)
		curr_ranges = new_ranges

	pr(curr_ranges)"""




	"""# not 20
	# not 0
	b = math.inf
	idx = 0
	while idx < len(seeds):
		m = seeds[idx]
		m2 = seeds[idx + 1]
		r = (m, m + m2 - 1)
		#pr(r)
		# split relative to all ranges in maps2[seeds]
		cr = conv_range(r, maps2)
		for r in cr:
			pr(r)
		#pr("CR", cr)
		b = min(b, min(r[0] for r in cr))"""
	"""for s in range(m, m + m2):
			if s % 1000000 == 0:
				pr(s)
			b = min(b, conv(s, maps))"""
	"""idx += 2
		pr(idx)
		pr("B", b)
	return b"""

"""def well_formed(r):
	return r[1] > r[0]

def conv_range(range1, maps):
	have = "seed"
	curr_ranges = [range1]
	while have != "location":
		new_ranges = []

		for r in maps[have][1]:
			for r2 in curr_ranges:
				if r[1] < r2[0] or r[0] > r2[1]:
					continue
				before = (r2[0], min(r2[1], r[0] - 1))
				within = (max(r[0], r2[0]), min(r[1], r2[1]))
				after = (max(r2[0], r[1] + 1), r2[1])
				#pr("after-split", r, r2, before, within, after)
				#new = list(filter(well_formed, [before, within, after]))
				# now apply modifier
				if well_formed(before):
					new_ranges.append(before)
				if well_formed(after):
					new_ranges.append(after)
				if well_formed(within):
					new_ranges.append((within[0] + r[2], within[1] + r[2]))
				#for nr in new:
				#	new_ranges.append((nr[0] - r[2], nr[1] - r[2]))
		curr_ranges = new_ranges
		have = maps[have][0]
		#if have != "location":
		#	pr(curr_ranges, have, maps[have][1])










		for r2 in curr_ranges:
			any_overlaps = False
			for r in maps[have][1]:
				if r[1] < r2[0] or r[0] > r2[1]:
					continue
				any_overlaps = True
				before = (r2[0], min(r2[1], r[0] - 1))
				within = (max(r[0], r2[0]), min(r[1], r2[1]))
				after = (max(r2[0], r[1] + 1), r2[1])
				#pr("after-split", r, r2, before, within, after)
				#new = list(filter(well_formed, [before, within, after]))
				# now apply modifier
				if well_formed(before):
					new_ranges.append(before)
				if well_formed(after):
					new_ranges.append(after)
				if well_formed(within):
					new_ranges.append((within[0] + r[2], within[1] + r[2]))
				#for nr in new:
				#	new_ranges.append((nr[0] - r[2], nr[1] - r[2]))
			if not any_overlaps:
				new_ranges.append(r2)
		curr_ranges = new_ranges
		have = maps[have][0]
		#if have != "location":
		#	pr(curr_ranges, have, maps[have][1])
	return curr_ranges"""

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
