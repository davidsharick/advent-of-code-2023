#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """#.#####################
#.......#########...###
#######.#########.#.###
###.....#.>.>.###.#.###
###v#####.#v#.###.#.###
###.>...#.#.#.....#...#
###v###.#.#.#########.#
###...#.#.#.......#...#
#####.#.#.#######.#.###
#.....#.#.#.......#...#
#.#####.#.#.#########v#
#.#...#...#...###...>.#
#.#.#v#######v###.###v#
#...#.>.#...>.>.#.###.#
#####v#.#.###v#.#.###.#
#.....#...#...#.#.#...#
#.#########.###.#.#.###
#...###...#...#...#.###
###.###.#.###v#####v###
#...#...#.#.>.>.#.>.###
#.###.###.#.###.#.#v###
#.....###...###...#...#
#####################.#"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 94)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 154)
		pass

def parse_input(inp, p2mode = False):
	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	def pf(p0, p1):
		y0, x0 = p0
		y1, x1 = p1
		if inp[y0][x0] == "." and inp[y1][x1] in ".v^<>":
			return 1
		elif inp[y0][x0] == ">" and inp[y1][x1] == "." and y0 == y1 and x0 == x1 - 1:
			return 1
		elif inp[y0][x0] == "v" and inp[y1][x1] == "." and y0 == y1 - 1 and x0 == x1:
			return 1
		elif inp[y0][x0] == "<" and inp[y1][x1] == "." and y0 == y1 and x0 == x1 + 1:
			return 1
		elif inp[y0][x0] == "^" and inp[y1][x1] == "." and y0 == y1 + 1 and x0 == x1:
			return 1
		else:
			return -1
	g = grid.graph_from_grid(inp, pf, corners = False, allow_negative = False)
	for x in range(len(inp[0])):
		if inp[0][x] == ".":
			start = (0, x)
			break
	for x in range(len(inp[-1])):
		if inp[-1][x] == ".":
			end = (len(inp) - 1, x)
			break
	paths = [[start]]
	done = []
	while len(paths) > 0:
		np = []
		for p in paths:
			n = g.adj[p[-1]]
			for node in n:
				if node.dest == end:
					done.append(p)
					continue
				if node.dest not in p:
					np.append(p + [node.dest])
		paths = np
	return max(len(d) for d in done)

def longest(g, s, e, ignore):
	paths = [[s]]
	done = []
	while len(paths) > 0:
		np = []
		for p in paths:
			n = g.adj[p[-1]]
			for node in n:
				if node.dest == e:
					done.append(p)
					continue
				elif node.dest in ignore:
					continue
				if node.dest not in p:
					np.append(p + [node.dest])
		paths = np
	if len(done) == 0:
		return None
	return max(len(d) for d in done)

def part2(inp):
	def pf(p0, p1):
		y0, x0 = p0
		y1, x1 = p1
		if inp[y0][x0] in ".v^<>" and inp[y1][x1] in ".v^<>":
			return 1
		else:
			return -1
	g = grid.graph_from_grid(inp, pf, corners = False, allow_negative = False)
	for x in range(len(inp[0])):
		if inp[0][x] == ".":
			start = (0, x)
			break
	for x in range(len(inp[-1])):
		if inp[-1][x] == ".":
			end = (len(inp) - 1, x)
			break
	imp = [start, end]
	for y in range(len(inp)):
		for x in range(len(inp[0])):
			c = 0
			for n in grid.grid_neighbors((y, x), (0, len(inp) - 1), (0, len(inp[0]) - 1), corners = False):
				if inp[n[0]][n[1]] in "<>^v":
					c += 1
			if inp[y][x] in "." and c >= 3:
				imp.append((y, x))
	sedge = defaultdict(list)
	for i, n in enumerate(imp):
		for n2 in imp[i + 1:]:
			d = longest(g, n, n2, imp)
			if d is not None:
				sedge[n].append(graph.Edge(n2, d))
				sedge[n2].append(graph.Edge(n, d))
	g2 = graph.Graph(imp, sedge)
	paths = [([start], 0)]
	done = []
	while len(paths) > 0:
		bests = Counter()
		if len(paths) >= 50:
			pass
		np = []
		mx = max(p[1] for p in paths)
		for path in paths:
			p, d = path
			c = (frozenset(p), p[-1])
			if bests[c] > d or (mx >= 100 and d <= mx / 1.4): # Empirically derived heuristic, objectively flawless
				continue
			bests[c] = d
			n = g2.adj[p[-1]]
			for node in n:
				if node.dest == end:
					done.append((p, d + node.cost))
					continue
				if node.dest not in p:
					np.append((p + [node.dest], d + node.cost))
		paths = np
	return max(d[1] for d in done)

# above 5562
# above 6254
# above 6390

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
