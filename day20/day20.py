#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 11687500)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	flips = defaultdict(list)
	conjs = defaultdict(list)
	broads = []
	o = []
	for idx, line in enumerate(inp):
		tokens = line.strip().split()
		if tokens[0].startswith("%"):
			label = tokens[0][1:]
			for t in tokens[2:]:
				flips[label].append(t.strip(","))
		elif tokens[0].startswith("&"):
			label = tokens[0][1:]
			for t in tokens[2:]:
				conjs[label].append(t.strip(","))
		elif tokens[0] == "broadcaster":
			for t in tokens[2:]:
				broads.append(t.strip(","))
	return flips, conjs, broads

	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(line.strip())
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def part1(inp):
	flips, conjs, broad = inp
	flip_states = {}
	conj_mem = defaultdict(dict)
	for f in flips.keys():
		flip_states[f] = False
		for o in flips[f]:
			if o in conjs.keys():
				conj_mem[o][f] = False
	for k, v in conjs.items():
		for o in v:
			if o in conjs.keys():
				conj_mem[o][k] = False
	lows = 0
	highs = 0
	for _ in range(1000):
		pulse_queue = [("broadcaster", False, "button")]
		while len(pulse_queue) > 0:
			nxt = pulse_queue.pop(0)
			dst, typ, src = nxt
			if typ == True:
				highs += 1
			else:
				lows += 1
			if dst == "broadcaster":
				for b in broad:
					pulse_queue.append((b, typ, dst))
			elif dst in flips.keys():
				if typ == False:
					flip_states[dst] = not flip_states[dst]
					for f2 in flips[dst]:
						pulse_queue.append((f2, flip_states[dst], dst))
			elif dst in conjs.keys():
				conj_mem[dst][src] = typ
				snd = not all(v for v in conj_mem[dst].values())
				for f2 in conjs[dst]:
					pulse_queue.append((f2, snd, dst))
	return lows * highs


def part2(inp):
	# rx turns on when vr has received high most recently from all inputs
	# those are all conj: bm, cl, tn, dr
	# bm receives from conj ds
	# cl from conj dt
	# tn from conj bd
	# dr from conj cs







	flips, conjs, broad = inp
	flip_states = {}
	conj_mem = defaultdict(dict)
	for f in flips.keys():
		flip_states[f] = False
		for o in flips[f]:
			if o in conjs.keys():
				conj_mem[o][f] = False
	for k, v in conjs.items():
		for o in v:
			if o in conjs.keys():
				conj_mem[o][k] = False
	seens = defaultdict(list)
	lows = 0
	highs = 0
	for i in range(100000000000000000000):
		if i % 100000 == 0 and i > 0:
			cycles = []
			for k, v in seens.items():
				v2 = []
				for i in range(len(v) - 1):
					v2.append(v[i + 1] - v[i])
				cycles.append(v2[0])
			return math.lcm(*cycles)
		if i > 0 and all(v == False for v in flip_states.values()) and all(all(v == False for v in d.values()) for d in conj_mem.values()):
			#pr(i)
			assert False
		pulse_queue = [("broadcaster", False, "button")]
		while len(pulse_queue) > 0:
			nxt = pulse_queue.pop(0)
			dst, typ, src = nxt
			if src in ["bm", "cl", "tn", "dr"] and typ == True:
				seens[src].append(i + 1)
			if typ == True:
				highs += 1
			else:
				lows += 1
			if dst == "rx" and typ == False:
				return i + 1
			if dst == "broadcaster":
				for b in broad:
					pulse_queue.append((b, typ, dst))
			elif dst in flips.keys():
				if typ == False:
					flip_states[dst] = not flip_states[dst]
					for f2 in flips[dst]:
						pulse_queue.append((f2, flip_states[dst], dst))
			elif dst in conjs.keys():
				conj_mem[dst][src] = typ
				snd = not all(v for v in conj_mem[dst].values())
				for f2 in conjs[dst]:
					pulse_queue.append((f2, snd, dst))
	#pr(lows, highs)
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
