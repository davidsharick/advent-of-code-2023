#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import copy
import hashlib
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.const as const
import utils.data as data
#import utils.direction as direc
import utils.frac as frac
import utils.graph as graph
import utils.grid as grid
import utils.lines as lines
import utils.ocr as ocr
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 405)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 400)
		pass

def parse_input(inp, p2mode = False):
	o = []
	curr = []
	for idx, line in enumerate(inp):
		if len(line.strip()) == 0:
			o.append(curr)
			curr = []
		else:
			curr.append(list(line.strip()))
	o.append(curr)
	return o

	return readin.int_single_input(inp) #123
	return readin.int_list_single_space_separated_input(inp) #123 456
	return readin.int_list_single_comma_separated_input(inp) #123,456
	return readin.int_list_single_arbitrary_separated_input(inp, "x") #123x456

	return readin.int_list_multiple_input(inp) # 123\n456
	return readin.int_list_multiple_space_separated_input(inp) # 123 456\n789 012
	return readin.int_list_multiple_arbitrary_separated_input(inp, "x") # 123x456\n789x012

	return readin.string_single_input(inp) #string
	return readin.string_list_input(inp) # string\ntext

	return readin.grid_input(inp)
	return readin.grid_set_input(inp, "#")
	
	return readin.int_string_input(inp) #123456 -> [1, 2, 3, 4, 5, 6]
	return readin.int_list_input(inp) # 123\n456
	return readin.chars_input(inp) #ABC -> [A, B, C]

def summarize(pattern, ignore = -1):
	# try y-refl
	for y in range(1, len(pattern[0]) + 1):
		left = y
		right = len(pattern[0]) - y
		if left > right:
			if all(list(reversed(r[-right:])) == r[-(right * 2):-right] for r in pattern):
				if left != ignore:
					return left
		else:
			if all(list(reversed(r[:left])) == r[left:left * 2] for r in pattern):
				if left != ignore:
					return left
	
	for x in range(1, len(pattern)):
		top = x
		bottom = len(pattern) - x
		if top > bottom:
			if all(pattern[top - x2 - 1] == pattern[top + x2] for x2 in range(bottom)):
				if 100 * top != ignore:
					return 100 * top
		else:
			if all(pattern[top - x2 - 1] == pattern[top + x2] for x2 in range(top)):
				if 100 * top != ignore:
					return 100 * top
	return -1

def part1(inp):
	o = 0
	for n in inp:
		x = summarize(n)
		assert x > 0
		o += x
	return o

def part2(inp):
	o = 0
	for n in inp:
		orig = copy.deepcopy(n)
		done = False
		wrong = summarize(n)
		for y in range(len(n)):
			for x in range(len(n[0])):
				curr = copy.deepcopy(orig)
				curr[y][x] = "#" if orig[y][x] == "." else "."
				val = summarize(curr, ignore = wrong)
				if val > 0 and val != wrong:
					o += val
					done = True
					break
			if done:
				break
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
